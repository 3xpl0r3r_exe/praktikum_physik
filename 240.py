import abc
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt



def load_data(data):

    df = pd.read_csv(data, sep=";", encoding="utf8")
    df = df.apply(lambda x: x.str.replace(",", "."))


    #### Konstanten ######
    n = 1000 # Widungen der Spulen
    l = 477*10**(-3) # Laenge des Eisen
    d = 2*10**(-3) # Spaltablstand
    mu_null = 1.256637*10**(-6)

    ###### Fehler ########
    l_err = 0.0004
    d_err = 0.00005
    #### Sonde err = wert * 0.03
    #### Strom err = wert * 0.02




    df["Strom [A]"] = pd.to_numeric(
        df["Strom [A]"], errors="coerce"
    ).fillna(0, downcast="infer")
    df["magnetische Flussdichte [10^-3 T]"] = pd.to_numeric(df["magnetische Flussdichte [10^-3 T]"], errors="coerce").fillna(
        0, downcast="infer"
    )

    df['magnetische Feldstaerke H [A/m]'] = (n*df['Strom [A]'])/l - ((df['magnetische Flussdichte [10^-3 T]'])*10**(-3)*d)/(mu_null*l)

    df['Strom err [A]'] = abs(df["Strom [A]"] * 0.02)
    df['magnetische Flussdichte err [T]'] = abs(df["magnetische Flussdichte [10^-3 T]"] * 10**(-3) * 0.03)

    df['magnetische Feldstaerke err [A/m]'] = (((df['Strom err [A]']*n)/l)**2 + ((-n*df["Strom [A]"]*l_err)/(l**2) + (d*df["magnetische Flussdichte [10^-3 T]"]*10**(-3)*l_err)/(mu_null*l**2))**2 + ((d_err*df["magnetische Flussdichte [10^-3 T]"]*10**(-3))/(mu_null*l))**2 + ((df['magnetische Flussdichte err [T]']*d)/(mu_null*l))**2)**(1/2)



    return df

def plot_hysterese(data, name, show=True, safe=True):

    fig, ax = plt.subplots()
    plt.style.use("seaborn")

    x_data = np.array(data["magnetische Feldstaerke H [A/m]"].tolist())
    y_data = np.array(data["magnetische Flussdichte [10^-3 T]"].tolist()) * 10**(-3)
    x_error = np.array(data["magnetische Feldstaerke err [A/m]"].tolist())
    y_error = np.array(data["magnetische Flussdichte err [T]"].tolist())
    ax.errorbar(
        y=y_data[85:],
        x=x_data[85:],
        yerr=y_error[85:],
        xerr=x_error[85:],
        color="red",
        ls=" ",
        marker="x",
        ecolor="blue",
        elinewidth=1,
        label='aeussere Hystereseschleife',
    )
    ax.errorbar(
        y=y_data[:85],
        x=x_data[:85],
        yerr=y_error[:85],
        xerr=x_error[:85],
        color="red",
        ls=" ",
        marker="x",
        ecolor="black",
        elinewidth=1,
        label='Neukurve',
    )


    ax.set_xlabel("magnetische Feldstärke " + r"$H_{Fe}$" + " in " + r"$\frac{A}{m}$", fontsize=14)
    ax.set_ylabel("magnetische Flussdichte " + r"$B$" + " in " + r"$T$", fontsize=14)

    ax.legend()
    fig.tight_layout()


    if safe == True:
        plt.savefig(name, bbox_inches="tight")

    if show == True:
        plt.show()


def f(z, x):
    return (z[0]*x**8 + z[1]*x**7 + z[2]*x**6 + z[3]*x**5 + z[4]*x**4 + z[5]*x**3 + z[6]*x**2 + z[7]*x**1 + z[8])

def fit_neukurve_anf_per(data):
    x = data['magnetische Feldstaerke H [A/m]']
    y = data['magnetische Flussdichte [10^-3 T]'] * 10**(-3)
    x_error = data['magnetische Feldstaerke err [A/m]']
    y_error = data["magnetische Flussdichte err [T]"]
    z = np.polyfit(x,y,8, w=(1/y_error))
    c = np.arange(-100, 600, 0.2)
    p = z[0]*c**8 + z[1]*c**7 + z[2]*c**6 + z[3]*c**5 + z[4]*c**4 + z[5]*c**3 + z[6]*c**2 + z[7]*c**1 + z[8]

    a = 0
    h = 0.0000000001
    b = 400
    print
    p_prime = (f(z, a+h) - f(z,a))/h

    st = (f(z,b) - f(z,a))/abs(b-a)

    

    
    tangent = f(z, a)+ p_prime*(c-a)
    tangent1 = f(z, a)+ p_prime*0.6*(c-a)
    tangent2 = f(z, a)+ p_prime*1.4*(c-a)
    tangent3 = st*c + 0.008
    tangent4 = st*1.08*c + 0.008
    tangent5 = st*0.92*c + 0.008
    print(z)

    print('Steigung: ' + str(p_prime) + ' +- ' + str((p_prime * 0.4)))
    print('Steigung: ' + str(st) + ' +- ' + str((st * 0.08)))

    fig, ax = plt.subplots()
    plt.style.use("seaborn")

    ax.errorbar(
        y=y,
        x=x,
        yerr=y_error,
        xerr=x_error,
        color="red",
        ls=" ",
        marker="x",
        capsize=2,
        capthick=1,
        ecolor="black",
        label='unterer Teil Neukurve'
    )
    ax.plot(c,p, label='Polynomfit 8. Grades durch Neukurve')
    
    ax.plot(c,tangent, label='Anfangspermeabilitaet ' + r'$\mu_{A}$')
    ax.plot(c,tangent1, linestyle='--')
    ax.plot(c,tangent2, linestyle='--')
    ax.plot(c, tangent3, label='maximale Permeabilitaet ' + r'$\mu_{max}$')
    ax.plot(c,tangent4, linestyle='--')
    ax.plot(c,tangent5, linestyle='--')   

    ax.set_xlabel("magnetische Feldstärke " + r"$H_{Fe}$" + " in " + r"$\frac{A}{m}$", fontsize=14)
    ax.set_ylabel("magnetische Flussdichte " + r"$B$" + " in " + r"$T$", fontsize=14)

    ax.legend()
    fig.tight_layout()
    plt.ylim([0, 0.25])
    plt.savefig('neuk.png', bbox_inches="tight")
    #plt.show()
    #plt.xlim([-40, 50])
    #plt.ylim([0, 0.03])
    #plt.savefig('Anfangspermeabilitaet.png', bbox_inches="tight")
    #plt.show()





if __name__ == '__main__':

    df = load_data('hysteres.csv')
    fit_neukurve_anf_per(df)

    #print(df.head(10))



    #out = df.to_csv('output.csv', sep=';', encoding='utf8')


    #plot_hysterese(df, 'plot.png')
    #print(df.to_latex())




