import numpy as np
import pandas as pd
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
import uncertainties as unc
import uncertainties.unumpy as unp


class Fit:
    def __init__(self, data, form, x='x_value', y='y_value', yerr='y_error'):
        self.data = data

        self.form = form  # linear, exponential, quadratic

        ## Als Daten muss eine CSV-Datei angegeben werden mit den Spalten: 'x_value', 'y_value', 'y_error'

        ## Gucke wie die y-Werte von x abhaengen und transformiere gegebenfalls die y-Werte und die y-Fehler
        if self.form == "exponential":
            self.x_data = np.array(self.data[x].tolist())
            self.y_data = np.array(self.data[y].tolist())
            self.y_errors = np.array(self.data[yerr].tolist())
            self.y_errors = abs(
                (
                    (
                        np.log(self.y_data - self.y_errors)
                        - np.log(self.y_data + self.y_errors)
                    )
                    / 2
                )
            )
            self.y_data = np.log(self.y_data)

        elif self.form == "quadratic":
            self.x_data = np.array(self.data[x].tolist())
            self.y_data = np.array(self.data[y].tolist())
            self.y_errors = np.array(self.data[yerr].tolist())
            self.y_errors = abs(
                (
                    np.sqrt((self.y_data - self.y_errors)) 
                    - np.sqrt((self.y_data + self.y_errors))
                )
                / 2
            )
            self.y_data = self.y_data**(1/2)

        elif self.form == "linear":
            self.x_data = np.array(self.data[x].tolist())
            self.y_data = np.array(self.data[y].tolist())
            self.y_errors = np.array(self.data[yerr].tolist())

        else:
            raise Exception("Keine gültige Form angegeben")

    def generate_table(
        self, title=["X-Data", "Y-Data", "Y-Errors"]
    ):  # gibt einen Dataframe mit individuellen Ueberschriften zurueck, y-Werte und Fehler werden automatisch gerundet

        y = []
        err = []
        for c in range(len(self.y_data)):
            y.append(round(self.y_data[c], self.find_dec(self.y_errors[c])))
            err.append(round(self.y_errors[c], self.find_dec(self.y_errors[c])))


        d = {title[0]: self.x_data, title[1]: y, title[2]: err}

        df = pd.DataFrame(d)

        return df

    def linear_function(self, x, m, b):  # notwendig fuer curve_fit
        return m * x + b

    def find_dec(self, error): # Legt fest auf welche Dezimalstelle m und b gerundent werden, dafuer wird der Fehler von m und b angeguckt
        try:
            er = str(error).split('.')[1]
            i = 1
            for x in er:
                if x != '0':
                    return i + 1
                else:
                    i = i + 1
        except:
            return int(1)
            
        

    def fit(self):  # fuehre Fit durch
        parameters, cov = curve_fit(
            self.linear_function,
            self.x_data,
            self.y_data,
            sigma=self.y_errors,
            absolute_sigma=True,
        )

        m = parameters[0]
        b = parameters[1] # m = popt[0]

        m_err = cov[0, 0] ** 0.5  # Berechne Fehler aus Varianzen
        b_err = cov[1, 1] ** 0.5



        return {
            "m": m,
            "b": b,
            "m_error": m_err,
            "b_error": b_err,
        }, parameters, cov  # gibt Dictionary mit den Parametern und ihren Fehlern zurück

    def tmp_f(self, x, c, m):
        return c + m * x

    def show_plot_save_plot(
        self, xlabel, ylabel, path
    ):  # zeigt den Plot und speichert ihn im gleichen Verzeichnis ab
        fit, popt, pcov = self.fit()
        y_fit = fit["m"] * self.x_data + fit["b"]

        dec_m = self.find_dec(str(fit['m_error']))
        dec_b = self.find_dec(str(fit['b_error']))
 

        fig, ax = plt.subplots()
        plt.style.use("seaborn")












        ####
        a, b = unc.correlated_values(popt, pcov)
        px = np.linspace(self.x_data[0], self.x_data[-1])
        py = a*px + b
        nom = unp.nominal_values(py)
        std = unp.std_devs(py)



        ax.fill_between(px,
                 nom + std,
                 nom - std,
                 alpha=0.3, color="green")
        #ax.plot(px, nom + std, color="green")
        #ax.plot(px, nom - std, color="green")

        ####
        ax.plot(
            self.x_data,
            y_fit,
            lw=2,label="Fit with Uncertainty Band"
            )
        ax.errorbar(
            self.x_data,
            self.y_data,
            yerr=self.y_errors,
            color="red",
            ls="",
            #marker="o",
            capsize=2,
            capthick=1,
            ecolor="red",
            label="Datenpunkte",
        )
        ax.set_xlabel(str(xlabel), fontsize=14)
        ax.set_ylabel(str(ylabel), fontsize=14)
        ax.legend()
        fig.tight_layout()
        # plt.show()
        plt.savefig(str(path), bbox_inches="tight",dpi=300)
        plt.show()

    



if __name__ == "__main__":

    data = pd.read_csv('fd_data.csv', sep=';', encoding='utf8') # import data
    #data['I'] = data['I_1'] + data['I_2']
    #data['x_value'] = data['U'] 
    #data['y_value'] = (((data['r2'] - data['r1'])*10**(-2))**2)*data['I']**2
    #data['y_error'] = ((2*(10**-2)*(data['r2'] - data['r1'])*3*(10**-3)*data['I']**2)**2 + (2*data['I']*0.02*((10**-2)*(data['r2'] - data['r1']))**2)**2)**(1/2)





    f = Fit(data, 'linear')
    a = f.fit()
    f.show_plot_save_plot(r"U" + '  ' + r'[$V$]', r'$(I\cdot r)^2$' + '  ' + r'[$(A\cdot m)^2$]', 'plot2.png')

    print(data)
    print(a)
    out = data.to_csv('output.csv', sep=';', encoding='utf8')







