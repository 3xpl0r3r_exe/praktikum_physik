from fit import Fit
import pandas as pd


if __name__ == '__main__':

    data = pd.read_csv('fd_data.csv', sep=';', encoding='utf8')
    data['I'] = data['I_1'] + data['I_2']
    data['x_value'] = data['U'] 
    data['y_value'] = (((data['r2'] - data['r1'])*10**(-2))**2)*data['I']**2
    data['y_error'] = ((2*(10**-2)*(data['r2'] - data['r1'])*3*(10**-3)*data['I']**2)**2 + (2*data['I']*0.02*((10**-2)*(data['r2'] - data['r1']))**2)**2)**(1/2)





    f = Fit(data, 'linear')
    a = f.fit()
    f.show_plot_save_plot(r"U" + '  ' + r'[$V$]', r'$(I\cdot r)^2$' + '  ' + r'[$(A\cdot m)^2$]', 'plot.png')

    print(data)
    print(a)
    out = data.to_csv('out.csv', sep=';', encoding='utf8')
